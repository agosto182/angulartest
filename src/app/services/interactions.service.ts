import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class InteractionsService{
	public url:string;
	
	constructor(private _http: Http){
		this.url=GLOBAL.url;
	}

	search(filter,start=0,limit=10){
        let params="limit="+limit+"&skip="+start;
        
		if(filter.comments!=0){
            params+="&comento="+filter.comments;
        }
        if(filter.likes!=0){
            params+="&like_post="+filter.likes;
        }
        if(filter.loves!=0){
            params+="&love_post="+filter.loves;
        }
        if(filter.hahas!=0){
            params+="&haha_post="+filter.hahas;
        }
        if(filter.wows!=0){
            params+="&wow_post="+filter.wows;
        }
        if(filter.sads!=0){
            params+="&sad_post="+filter.sads;
        }
        if(filter.angries!=0){
            params+="&angry_post="+filter.angries;
        }
        if(filter.comps!=0){
            params+="&comp_post="+filter.comps;
        }
        if(filter.exclude==1){
            params+="&excl=1";
        }
        
        if(filter.likes_com!=0){
            params+="&like_comentario="+filter.likes_com;
        }
        if(filter.loves_com!=0){
            params+="&love_comentario="+filter.loves_com;
        }
        if(filter.hahas_com!=0){
            params+="&haha_comentario="+filter.hahas_com;
        }
        if(filter.wows_com!=0){
            params+="&wow_comentario="+filter.wows_com;
        }
        if(filter.sads_com!=0){
            params+="&sad_comentario="+filter.sads_com;
        }
        if(filter.angries_com!=0){
            params+="&angry_comentario="+filter.angries_com;
        }
        console.log(params);
        
		return this._http.get(this.url+params).map(res=>res.json());
	}
}