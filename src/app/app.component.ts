import { Component } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import {InteractionsService} from './services/interactions.service';
import {GoogleCharts} from 'google-charts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [InteractionsService]
})
export class AppComponent {
  title = 'app';
  public page=1;
  public total=0;
  public start=0;
  public perPage=10;
  public pages=[];
  public isCharging=false;
  public filters={
    comments:0,
    likes:0,
    loves:0,
    hahas:0,
    wows:0,
    sads:0,
    angries:0,
    comps:0,
    exclude:0,
    likes_com:0,
    loves_com:0,
    hahas_com:0,
    wows_com:0,
    sads_com:0,
    angries_com:0,
  };

  public graphic={
    likes:0,
    loves:0,
    hahas:0,
    wows:0,
    sads:0,
    angries:0
  };
  public result=[];

  constructor(private _interactionsService:InteractionsService){
    this.onSubmit();
  }

  onSubmit(){
    this.isCharging=true;
    let dat=this._interactionsService.search(this.filters,this.start,this.perPage).subscribe((res)=>{
      this.result=res.users;
      this.total=res.countResp;
      this.makePages();
      this.makeChart();
    });
  }

  private makePages(){
    let pages=this.total/this.perPage;
    this.pages=[];
    for (let index = 1; index <= 6; index++) {
      if(index==pages){
        break;
      }
      this.pages.push(index);
    }
    console.log(pages);
    this.isCharging=false;
  }

  linkClass(index){
    if(index==this.page){
      
    }
  }
  goPage(page){
    this.page=page;
    this.start=this.perPage*(this.page-1);
    this.onSubmit();
  }

  makeChart(){
    let graphic={
      likes:0,
      loves:0,
      hahas:0,
      wows:0,
      sads:0,
      angries:0,
      likes_com:0,
      loves_com:0,
      hahas_com:0,
      wows_com:0,
      sads_com:0,
      angries_com:0
    };
    this.result.forEach(element => {
      if(element.like_post){
        graphic.likes+=element.like_post;
      }
      if(element.love_post){
          graphic.loves+=element.love_post;
      }
      if(element.haha_post){
          graphic.hahas+=element.haha_post;
      }
      if(element.wow_post){
          graphic.wows+=element.wow_post;
      }
      if(element.sad_post){
          graphic.sads+=element.sad_post;
      }
      if(element.angry_post){
          graphic.angries+=element.angry_post;
      }
      
      if(element.like_comentario){
        graphic.likes_com+=element.like_comentario;
      }
      if(element.love_comentario){
          graphic.loves_com+=element.love_comentario;
      }
      if(element.haha_comentario){
          graphic.hahas_com+=element.haha_comentario;
      }
      if(element.wow_comentario){
          graphic.wows_com+=element.wow_comentario;
      }
      if(element.sad_comentario){
          graphic.sads_com+=element.sad_comentario;
      }
      if(element.angry_comentario){
          graphic.angries_com+=element.angry_comentario;
      }
    });
    console.log(graphic);
    GoogleCharts.load(()=>{
      const data = GoogleCharts.api.visualization.arrayToDataTable([
          ['Tipo', 'Interacciones'],
          ['Likes', graphic.likes],
          ['Loves', graphic.loves],
          ['Hahas', graphic.hahas],
          ['Wows', graphic.wows],
          ['Sads', graphic.sads],
          ['Angries', graphic.angries]
      ]);
      const pie_1_chart = new GoogleCharts.api.visualization.PieChart(document.getElementById('interactions_grafic'));
      pie_1_chart.draw(data);
    });

    GoogleCharts.load(()=>{
      const data = GoogleCharts.api.visualization.arrayToDataTable([
          ['Tipo', 'Interacciones en comentarios'],
          ['Likes', graphic.likes_com],
          ['Loves', graphic.loves_com],
          ['Hahas', graphic.hahas_com],
          ['Wows', graphic.wows_com],
          ['Sads', graphic.sads_com],
          ['Angries', graphic.angries_com]
      ]);
      const pie_1_chart = new GoogleCharts.api.visualization.PieChart(document.getElementById('interactions_grafic'));
      pie_1_chart.draw(data);
    });
  }
}
